﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacManMovement : MonoBehaviour
{
    [SerializeField]
    float Movementspeed;
    public int TotalPoints;
    public int Lives = 3;

    AudioSource _audioSource;

    [SerializeField]
    AudioClip _audioStart;


    [SerializeField]
    AudioClip _audioMov;

    [SerializeField]
    AudioClip _audioDead;

    bool _isgameReady;

    public bool IsGameReady()
    {
        return _isgameReady;
    }
    bool hashpower;
    /// <summary>
    /// rappresenta il numero di fantasmi mangiati col power up 
    /// </summary>
    int _eatghost;

    /// <summary>
    /// Tempo trascorso da quando sie raccolto il power up.
    /// </summary>
    float powerUpElapsedTime = 0;
    /// <summary>
    /// Indica ultimo power up raccolto
    /// </summary>
    float _powerupDuration = 10;


    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();

    }

    // Use this for initialization
    void Start()
    {
        StartCoroutine(CheckStartMusic());

    }

    /// <summary>
    /// Aspetta che la musica di inizio sia terminata prima di permettere di muoverci
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckStartMusic()
    {
        _isgameReady = false;
        _audioSource.PlayOneShot(_audioStart);
        while (_audioSource.isPlaying)
            yield return null;
        _isgameReady = true;

    }
    // Update is called once per frame
    void Update()
    {

        if (_isgameReady == false)
            return;

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");


        //Debug.Log("h:" + h + "-v:" + v);



        if (h != 0)
        {


            transform.Translate(Vector3.right * Movementspeed * h);
        }

        else
        {

        }

        transform.Translate(Vector3.forward * Movementspeed * v);

        if (h != 0 || v != 0)
        {
            if (!_audioSource.isPlaying)
                _audioSource.PlayOneShot(_audioMov);
        }

        if (hashpower)
        {
            powerUpElapsedTime += Time.deltaTime;
            Debug.Log("Elapsed Time" + powerUpElapsedTime);
            if (powerUpElapsedTime >= _powerupDuration)
            {
                hashpower = false;
            }
        }

    }



    void OnTriggerEnter(Collider other)
    {


        if (other.gameObject.tag == "pill")
        {
            OnEatpill(other);
        }



        if (other.gameObject.tag == "ghost")
        {
            if (!hashpower)
            {
                Onhit();
            }


            else
            {


                _eatghost++;


                TotalPoints += (int)Mathf.Pow(2, _eatghost - 1) * Ghost.Points;
                Destroy(other.gameObject);
            }
        }

    }


    void OnEatpill(Collider other)
    {

        Debug.Log("GNAM");
        Pill pill = other.gameObject.GetComponent<Pill>();


        TotalPoints += pill.Points;

        if (pill is Powerup)
        {
            Debug.Log("Power up");

            powerUpElapsedTime = 0;
            hashpower = true;
        }
        Destroy(other.gameObject);
    }


    void Onhit()
    {
        Debug.Log("Game over");
        if (!_audioSource.isPlaying)


        {
            _audioSource.Stop();


        }
        _audioSource.PlayOneShot(_audioDead);



        Lives -= 1;
    }



}
