﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Ghost : MonoBehaviour
{
    NavMeshAgent _navAgent;
    GameObject _player;
    PacManMovement _pacman;


    public GameObject smash;

   static public int Points=200;

    void Awake()
    {

        _navAgent = GetComponent<NavMeshAgent>();
        
    }
    private void OnDisable()
    {
        Destroy(smash);
    }

    // Use this for initialization
    void Start ()
    {
        //gameObject.transform.rotation = Quaternion.AngleAxis(90, Vector3.right);
        
        _player = GameObject.FindGameObjectWithTag("Player");
        if (_player==null)
      {
            throw new UnityException("Player missing");
        }
        _pacman = _player.GetComponent<PacManMovement>();
       // _navAgent.SetDestination(_player.transform.position);
	}
	    
	// Update is called once per frame
	void Update () {
        if (_pacman.IsGameReady() == false)
            return;
        smash.transform.position = transform.position;


        _navAgent.SetDestination(_player.transform.position);
    }
}
